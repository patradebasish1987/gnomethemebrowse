# Gnome Themes Installer


## About

Presenting Gnome Themes Installer project which helps to install latest 50 themes for your Gnome Desktop Natively with libadwaita & Rust.

## Screenshots

![IMAGE_DESCRIPTION](https://gitlab.gnome.org/patradebasish1987/gnomethemebrowse/-/raw/main/screenshots/screenshot1.png)

![IMAGE_DESCRIPTION](https://gitlab.gnome.org/patradebasish1987/gnomethemebrowse/-/raw/main/screenshots/screenshot2.png)

## Authors and acknowledgment
Debasish Patra

## License
GPL-2
