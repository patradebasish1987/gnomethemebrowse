use adw::glib;
use adw::gtk::gdk;
use adw::gtk::CssProvider;
use adw::gtk::{self, Button, FlowBox, Label, Picture, ScrolledWindow};
use adw::gtk::{Box, ListBox};

use adw::prelude::*;
use adw::{ActionRow, Clamp, Dialog};
use glib::clone;

use base64::engine::general_purpose;

use reqwest::blocking::Client;
use serde_json::Value;
use soup::prelude::*;
use std::fmt;
use std::fs;
use std::fs::File;
use std::io::{Result, Write};

use tokio::runtime::Runtime;
use urlencoding::decode; // 0.3.5

#[derive(Debug, Clone, Copy)]
pub enum LinkType {
    IconTheme,
    GtkTheme,
    CursorTheme,
    GnomeShellTheme,
    Invalid,
}
#[derive(Debug, Clone)]
pub struct ProductCatalogType {
    pub title: String,
    pub project_id: String,
    pub changed_at: String,
    pub image_small: String,
    pub score: f32,
}
#[derive(Debug, Clone)]
pub struct ProjectDownloadLinks {
    pub title: String,
    pub filename: String,
    pub ftype: String,
    pub updated_timestamp: String,
    pub url: String,
    pub themetype: LinkType,
}
impl fmt::Display for LinkType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl fmt::Display for ProductCatalogType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Title : {} \nProject_ID : {}\nChanged : {}\nImage_url : {}",
            self.title, self.project_id, self.changed_at, self.image_small
        )
    }
}
pub fn get_products(url: &str) -> (LinkType, Vec<ProductCatalogType>) {
    use base64::Engine;
    let mut buffer = Vec::<u8>::new();
    let client = Client::new();
    let res = client
        .get(url)
        .send()
        .expect(format!("Invalid Url : {}", url).as_str())
        .text()
        .expect("Failed to get payload");

    let soup = Soup::new(res.as_str());
    let alltags = soup
        .tag("script")
        .find_all()
        .map(|a| a.display())
        .filter(|b| b.contains("productBrowseDataEncoded"))
        .collect::<Vec<_>>();
    //println!("All Data : {:#?}", alltags);
    let decodestr = &alltags[0];

    let start_idx = decodestr.find("productBrowseDataEncoded = '").unwrap()
        + "productBrowseDataEncoded = '".len();
    let end_idx = decodestr.find(";\n var catTree").unwrap() - 1;

    let base64json = &decodestr[start_idx..end_idx];

    general_purpose::STANDARD
        .decode_vec(&base64json, &mut buffer)
        .unwrap();

    let str = std::str::from_utf8(&buffer).unwrap();
    let products: Value = serde_json::from_str(str).unwrap();
    let prodthemetype = match products["catTitle"].as_str() {
        Some("Full Icon Themes") => LinkType::IconTheme,
        Some("Cursors") => LinkType::CursorTheme,
        Some("GTK3/4 Themes") => LinkType::GtkTheme,
        Some("Gnome Shell Themes") => LinkType::GnomeShellTheme,
        _ => LinkType::Invalid,
    };
    let products = &products["products"];
    let mut product_catalog: Vec<ProductCatalogType> = Vec::new();

    //println!("Product Array : {}", products.as_array().unwrap()[0]);
    //for  each_product in products.as_array(){
    if let Some(each_product) = products.as_array() {
        for each_idx in each_product {
            product_catalog.push(ProductCatalogType {
                title: each_idx["title"].as_str().unwrap().to_string(),
                project_id: each_idx["project_id"].as_str().unwrap().to_string(),
                changed_at: each_idx["changed_at"].as_str().unwrap().to_string(),
                image_small: each_idx["image_small"].as_str().unwrap().to_string(),
                score: (each_idx["laplace_score"]
                    .as_str()
                    .unwrap()
                    .to_string()
                    .parse::<f32>()
                    .unwrap())
                    / 100.0,
            });
        }
    }
    //let product_json = serde_json::to_string_pretty(&products[0]).unwrap();
    //product_catalog = product_catalog[0..1].to_vec(); // eemove when u want to process all
    (prodthemetype, product_catalog)
}

pub async fn get_links_from_product(
    project_id: &str,
    project_title: &str,
    theme_type: &LinkType,
) -> Vec<ProjectDownloadLinks> {
    let client = reqwest::Client::new();
    let mut url = String::from("https://www.gnome-look.org/p/");
    url.push_str(project_id);
    url.push_str("/loadFiles");
    //println!("Url: {}", url);
    let res = client
        .get(url)
        .send()
        .await
        .expect("Could not get links to download")
        .text()
        .await
        .expect("Failed to get payload");

    let all_products: Value = serde_json::from_str(res.as_str()).unwrap();
    let products = &all_products["files"];
    let all_product_list = products.as_array().unwrap();
    let mut product_list: Vec<ProjectDownloadLinks> = Vec::new();
    for each_rec in all_product_list {
        if each_rec["active"].eq("1") {
            product_list.push(ProjectDownloadLinks {
                title: project_title.to_string(),
                filename: each_rec["title"].as_str().unwrap().to_string(),
                ftype: each_rec["type"].as_str().unwrap().to_string(),
                updated_timestamp: each_rec["updated_timestamp"].as_str().unwrap().to_string(),
                url: decode(each_rec["url"].as_str().unwrap())
                    .expect("Invalid URL for Download")
                    .to_string(),
                themetype: theme_type.clone(),
            });
        }
    }
    //println!("{:#?}", product_list);
    product_list
}

pub async fn fetch_url(url: String, file_name: String) -> Result<()> {
    let response = reqwest::get(&url).await; //.unwrap().bytes().await;
    match response {
        Ok(val) => match val.bytes().await {
            Ok(content) => {
                let path = std::path::Path::new(&file_name);

                let save_path = &file_name[0..file_name.rfind('/').unwrap()];
                //println!("New Save Dir : {}", save_path);
                let _ = fs::create_dir_all(save_path);

                let mut file = match File::create(&path) {
                    Err(why) => panic!("couldn't create {}", why),
                    Ok(file) => file,
                };
                //                let content = response;
                file.write_all(&content)?; //file.write_all(&content.unwrap()),
            }
            Err(e) => {
                panic!("Panic while converting to bytes : {} : {}", url, e);
            }
        },
        Err(e) => {
            panic!("Panic : {}", e);
        }
    }

    Ok(())
}
pub async fn install_theme(dndlink: ProjectDownloadLinks) -> Result<()> {
    //println!("Installing theme : {}", dndlink.themetype);

    let mut path = std::env::var("HOME").unwrap();
    path.push_str("/.cache/themeinstaller/");
    path.push_str(dndlink.themetype.to_string().as_str());
    path.push_str("/");

    let _r = fs::create_dir_all(&path.as_str());
    path.push_str(dndlink.filename.as_str());
    // Check if the file exists in cache, then skip download
    match std::path::Path::new(&path).exists() {
        true => {
            println!("File exists in cache : {} ", &path);
        }
        false => {
            let _res = fetch_url(dndlink.url, path.clone()).await;
        }
    }
    println!("File Downloaded. Going to install the file...");
    let _ = install_tar(
        &path.clone(),
        &dndlink.ftype.clone(),
        &dndlink.themetype.clone(),
    )
    .await
    .unwrap();
    println!("File Installed : {}", &path.clone());

    Ok(())
}

pub async fn install_tar(path: &str, _filetype: &str, theme_type: &LinkType) -> Result<()> {
    use std::process::Command;
    println!("Before installing tihe tar file. : {}", path);
    let mut extract_path = std::env::var("HOME").unwrap();
    match theme_type {
        LinkType::IconTheme | LinkType::CursorTheme => {
            extract_path.push_str("/.local/share/icons/");
        }
        LinkType::GtkTheme | LinkType::GnomeShellTheme => {
            extract_path.push_str("/.local/share/themes/");
        }
        _ => {}
    }
    //println!("Extracting Tar file : {} \nPath : {}", path, extract_path);
    if path.ends_with(".tar") || path.ends_with(".tar.xz") || path.ends_with(".tar.gz") || path.ends_with(".tar.bz2"){
        let _proc = Command::new("tar")
            .arg("-xf")
            .arg(&path)
            .arg("-C")
            .arg(&extract_path)
            .output()
            .expect("Failed to extract .tar/.tar.xz/.tar.gz not found");
    } else if path.ends_with(".7z") {
        //7z x <file_name>.tar.7z -opath
        let _proc = Command::new("7z")
            .arg("x")
            .arg(&path)
            .arg(format!("-o{}", &extract_path))
            //.arg(&extract_path)
            .output()
            .expect("Failed to extract .7z file");
    } else if path.ends_with(".zip") {
        //unzip file.zip -d destination_folder
        let _proc = Command::new("unzip")
            .arg(&path)
            .arg("-d")
            .arg(&extract_path)
            .output()
            .expect("Failed to extract .zip file");
    } else {
        println!("Unsupported file type. Didnt do anything...");
    }

    //println!("{} {} Installed Sucessfully", theme_type, &path);

    Ok(())
}
pub async fn downloadthumbs(products: Vec<ProductCatalogType>) -> Result<()> {
    //    let rt = tokio::runtime::Runtime::new().unwrap();
    //    rt.block_on(async {
    //async {
    println!("Got inside Download Thumbnail");
    let mut set = tokio::task::JoinSet::new();
    //let mut tasks = vec![]; //Vec::new();
    for each_product in products {
        //    tasks.push(tokio::spawn(async move {
        set.spawn(async move {
            //println!("In async tokio");
            let mut save_path = "/tmp/themeinstaller/cache/".to_string();
            save_path.push_str(&each_product.image_small);
            let mut save_dir = save_path.to_string();

            if !std::path::Path::new(&save_path).exists() {
                save_dir.push_str(&each_product.image_small);

                let save_dir = &save_dir[0..save_dir.rfind('/').unwrap()];
                let save_dir_copy = save_dir;
                let _ = fs::create_dir_all(&save_dir_copy);

                //println!("Save Dir : {}", &save_dir);

                //println!("Save Path :: {:?}", save_path);
                let mut image_link = "https://images.pling.com/cache/200x200-1/img/".to_string();
                image_link.push_str(&each_product.image_small);
                println!("Image Link : {}", image_link);

                fetch_url(image_link, save_path).await.unwrap();
            }
            println!("Got out after fetching Image Link");

            //let result = processed.await.unwrap();
            //result
            //}
            //      })i);
        });
    }
    while let Some(res) = set.join_next().await {
        let _out = res?;
        // ...
    }
    //    let results = futures_util::future::join_all(tasks).await;

    Ok(())

    //);
}

fn get_catalog_val_from_type(lk: &LinkType) -> u8 {
    let val = match lk {
        LinkType::GtkTheme => 135,
        LinkType::IconTheme => 132,
        LinkType::CursorTheme => 107,
        _ => 0,
    };
    val
}

fn build_url(link: &LinkType, pagenoval: &str, sortby: &str) -> String {
    format!(
        "https://www.gnome-look.org/browse?cat={}&page={}&ord={}",
        get_catalog_val_from_type(link),
        pagenoval,
        sortby
    )
}

pub fn create_catalog(link: LinkType, pagenoval: &str, sortby: &str) -> ScrolledWindow {
    //glib::timeout_future_seconds(1).await;
    // Doing background stuff while loading goes on for  theme
    let abs_save_dir = "/tmp/themeinstaller/thumbnails/";
    let _r = fs::create_dir_all(abs_save_dir);
    let url = build_url(&link, pagenoval, &sortby);
    println!(" Url : {}", url);
    let (_themetype, products) = match link {
        LinkType::IconTheme => get_products(&url),
        _ => {
            let (_, prd1) = get_products(&build_url(&link, "1", sortby));
            let (_, prd2) = get_products(&build_url(&link, "2", sortby));
            let (_, prd3) = get_products(&build_url(&link, "3", sortby));
            let (_, prd4) = get_products(&build_url(&link, "4", sortby));
            let (_, prd5) = get_products(&build_url(&link, "5", sortby));
            let products = [prd1, prd2, prd3, prd4, prd5].concat();
            (link, products)
        }
    };
    //let allproducts = products.clone();
    let _ = Runtime::new()
        .unwrap()
        .block_on(downloadthumbs(products.clone()));

    println!("Downloaded Products : {:#?}", &products);
    let scroll_flowbox = ScrolledWindow::new();
    //scroll_flowbox.set_vexpand(true);
    //scroll_flowbox.set_hexpand(true);
    scroll_flowbox.set_policy(gtk::PolicyType::Automatic, gtk::PolicyType::Automatic);
    //scroll_flowbox.set_propagate_natural_height(true);
    //scroll_flowbox.set_propagate_natural_width(true);

    let flowbox = FlowBox::builder()
        //.css_classes(vec!["flat"])
        //.hexpand_set(false).vexpand_set(false)
        .build();
    //flowbox.set_halign(gtk::Align::Center);
    flowbox.set_valign(gtk::Align::Start);

    scroll_flowbox.set_child(Some(&flowbox));

    for each_product in products {
        let eachproduct = Button::builder()
            .hexpand_set(false)
            .vexpand_set(false)
            .css_classes(vec![String::from("flat")])
            .valign(gtk::Align::Fill)
            .halign(gtk::Align::Fill)
            .build();

        eachproduct.set_size_request(200, 200);

        let flowchildbox = Box::builder()
            .hexpand_set(false)
            .vexpand_set(false)
            .valign(gtk::Align::Center)
            .halign(gtk::Align::Center)
            .build();
        eachproduct.set_child(Some(&flowchildbox));
        let mut imgpath = "/tmp/themeinstaller/cache/".to_string();

        imgpath.push_str(&each_product.image_small);

        let provider = CssProvider::new();
        //    provider.load_from_data(include_str!("style.css"));
        provider.load_from_data(".round{
	        border-radius: 15px;
	        border-width: 1px;
        }");

        // Add the provider to the default screen
        gtk::style_context_add_provider_for_display(
            &gdk::Display::default().expect("Could not connect to a display."),
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );

        let img = Picture::builder()
            .halign(gtk::Align::Center)
            .valign(gtk::Align::Center)
            .hexpand_set(true)
            .vexpand_set(true)
            .css_name("round")
            .build();

        img.add_css_class("round");

        img.set_filename(Some(&std::path::Path::new(imgpath.as_str())));
        img.set_size_request(200, 200);

        let imgclamp = Clamp::new();
        imgclamp.set_child(Some(&img));
        imgclamp.set_maximum_size(300);

        let imagelabel = Label::builder()
            .halign(gtk::Align::Center)
            .valign(gtk::Align::Center)
            .hexpand_set(true)
            .vexpand_set(true)
            .css_classes(vec![String::from("subtitle")])
            .build();

        imagelabel.set_wrap(true);
        imagelabel.set_justify(gtk::Justification::Center);
        imagelabel.set_single_line_mode(false);
        imagelabel.set_ellipsize(gtk::pango::EllipsizeMode::End);
        imagelabel.set_wrap_mode(gtk::pango::WrapMode::Char);
        imagelabel.set_max_width_chars(20);
        imagelabel.set_label(&each_product.title);

        let scorelabel = Label::builder()
            .halign(gtk::Align::Center)
            .css_classes(vec![String::from("subtitle")])
            .valign(gtk::Align::Center)
            .hexpand_set(false)
            .vexpand_set(false)
            .build();
        scorelabel.set_label(&each_product.score.to_string());

        let rowbox = ListBox::builder()
            .css_classes(vec![String::from("boxed-list")])
            .hexpand_set(true)
            .vexpand_set(true)
            .build();
        rowbox.set_show_separators(false);
        rowbox.append(&imgclamp);
        rowbox.append(&imagelabel);
        rowbox.append(&scorelabel);

        flowchildbox.append(&rowbox);
        flowbox.insert(&eachproduct, -1);
        let scroll_flowboxclone = scroll_flowbox.clone();
        let eachproductclone = eachproduct.clone();
        eachproductclone.connect_clicked(move |eachproductclone|{
                                println!("eachproductclone.connect_clicked");
                                  let prod_links = Runtime::new().unwrap().block_on(get_links_from_product(&each_product.project_id, &each_product.title, &link));
                                  println!("{:#?}",prod_links);

                                  let ad = Dialog::builder()
                                    //.body(&each_product.title).title("Installation")
                                    //.heading(&each_product.title)
                                    //.can_close(true)
                                    //.receives_default(true)
                                    .width_request(800)
                                    //.height_request(800)
                                    .vexpand_set(true)
                                    .hexpand_set(true)
                                    .build();
                                  //ad.set_heading(Some(&each_product.title));
                                  //ad.add_response("cancel", "Cancel");
                                  //ad.set_default_response(Some("cancel"));
                                  //ad.set_height_request(600);
                                  //ad.set_width_request(400);


                                    let linkbox = ListBox::builder()
                                        .css_classes(vec![String::from("boxed-list")])
                                        .margin_start(10)
                                        .margin_end(10)
                                        .margin_top(10)
                                        .margin_bottom(10)
                                        .vexpand(true)
                                        .hexpand(false)
                                        .build();
                                    linkbox.set_width_request(600);
                                 ad.set_child(Some(&Label::new(Some(&each_product.title))));
                                 ad.set_child(Some(&linkbox));


                                  glib::spawn_future_local(clone!(@weak  eachproductclone => async move {
                                  println!("Prod Links : {:#?}", prod_links);
                                  for each_link in prod_links{
                                      let downloadbutton = Button::builder().label("Download & Install").vexpand_set(false).valign(gtk::Align::Center).halign(gtk::Align::Center)
                                        .css_classes(vec![String::from("linked")])
                                        .build();
                                      let linkrow = ActionRow::builder().subtitle(&each_link.updated_timestamp).title(&each_link.filename).activatable(false).css_classes(vec![String::from("boxed-list")]).build();
                                      linkrow.add_suffix(&downloadbutton);
                                      linkbox.append(&linkrow);
                                      downloadbutton.connect_clicked(move|downloadbutton| {
                                          downloadbutton.set_label("Installing...");
                                          let _ = Runtime::new().unwrap().block_on(install_theme(each_link.clone()));
                                          downloadbutton.set_label("Installed");
                                          downloadbutton.set_sensitive(false);
                                      });

                                  }
                                  }));
                                  ad.set_visible(true);
                                  ad.present(&scroll_flowboxclone);
                      });
    }
    println!("Just Befoire Flowbox");
    scroll_flowbox
}
