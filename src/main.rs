pub mod helper;
use adw::glib;
use adw::gtk::gdk;
use adw::gtk::Box;
use adw::gtk::CssProvider;
//use adw::gtk::self;
use adw::gtk::Spinner;
use adw::gtk::{self, ListBox, ToggleButton};
use adw::prelude::*;
use adw::{
    ActionRow, Application, ApplicationWindow, HeaderBar, OverlaySplitView, ToolbarStyle,
    ToolbarView, ViewStack,
};
use glib::clone;

use crate::helper::*;

const APP_ID: &str = "org.scalpel.gnomethemesbrowser";


fn main() -> glib::ExitCode {
    // Create a new application
    let app = Application::builder().application_id(APP_ID)
        .build();
    adw::init().unwrap();

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn build_ui(app: &Application) {
    let provider = CssProvider::new();
    //    provider.load_from_data(include_str!("style.css"));
    //provider.load_from_path("style.css");
    provider.load_from_data(".round{
	    border-radius: 15px;
	    border-width: 1px;
    }");
    //provider.load_from_file("style.css");

    // Add the provider to the default screen
    gtk::style_context_add_provider_for_display(
        &gdk::Display::default().expect("Could not connect to a display."),
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    let headerwindow = adw::WindowTitle::new(
            "Gnome Themes Installer",
            "Installs Gtk, Icon & Cursor Themes",
        );

    let headerbar = HeaderBar::builder()
        .title_widget(&headerwindow)
        .build();


    //appbody.append(&headerbar);

    //let sidebarbox = Box::builder().halign(gtk::Align::End).hexpand_set(true).css_name("sidebar-view").build();
    let sidebarbox = ListBox::builder()
        .margin_top(5)
        .margin_end(5)
        .margin_bottom(5)
        .margin_start(5)
        .selection_mode(gtk::SelectionMode::Single)
        .css_classes(vec![String::from("boxed-list")])
        .halign(gtk::Align::Fill)
        .hexpand_set(true)
        .valign(gtk::Align::Start)
        .vexpand_set(false)
        .build();

    let gtkmenu = ActionRow::builder()
        .activatable(true)
        .title("Gtk Themes")
        .subtitle("Install Gtk 3/4 Themes")
        //        .hexpand_set(true)
        .build();
    /*
    gtkmenu.connect_activated(|_| {
        eprintln!("Gtk 3/4 Themes Clicked!");
    });
    */
    let iconmenu = ActionRow::builder()
        .activatable(true)
        .title("Icon Themes")
        .subtitle("Download Full Icon Themes")
        .build();
    let cursormenu = ActionRow::builder()
        .activatable(true)
        .title("Cursor Themes")
        .subtitle("Install Cursor Themes")
        .build();

    sidebarbox.append(&gtkmenu);
    sidebarbox.append(&iconmenu);
    sidebarbox.append(&cursormenu);

    let gtkcontentbox = Box::builder()
        .vexpand(true)
        .hexpand(true)
        .css_name("content-view")
        .build();
    let iconcontentbox = Box::builder()
        .vexpand(true)
        .hexpand(true)
        .css_name("content-view")
        .build();
    let cursorcontentbox = Box::builder()
        .vexpand(true)
        .hexpand(true)
        .css_name("content-view")
        .build();
    //    contentbox.append(&Label::new(Some("Content")));

    let loadingicon = Spinner::builder()
        .css_classes(vec![String::from("spinner")])
        .build();
    loadingicon.set_spinning(true);
    loadingicon.set_vexpand(true);
    loadingicon.set_hexpand(true);
    loadingicon.set_valign(gtk::Align::Center);
    loadingicon.set_halign(gtk::Align::Center);

    let viewstack = ViewStack::builder()
        .halign(gtk::Align::Baseline)
        .valign(gtk::Align::Baseline)
        .hexpand_set(true)
        .vexpand_set(true)
        .margin_top(0)
        .margin_bottom(0)
        .margin_start(0)
        .margin_end(0)
        .hhomogeneous(true)
        .vhomogeneous(true)
        .build();

    gtkcontentbox.append(&loadingicon);
    iconcontentbox.append(&loadingicon);
    cursorcontentbox.append(&loadingicon);

    viewstack.add_named(&gtkcontentbox, Some("contentboxloading"));
    viewstack.add_named(&iconcontentbox, Some("contentboxloading"));
    viewstack.add_named(&cursorcontentbox, Some("contentboxloading"));

    let osv = OverlaySplitView::builder()
        .css_name("navigation-split-view")
        .sidebar(&sidebarbox)
        .content(&viewstack)
        .build();

    let tbv = ToolbarView::builder()
        .top_bar_style(ToolbarStyle::Raised)
        .build();
    tbv.add_top_bar(&headerbar);
    tbv.set_extend_content_to_top_edge(false);
    tbv.set_extend_content_to_bottom_edge(true);

    let togglebut = ToggleButton::builder().css_classes(["toggle"]).build();
    //togglebut.set_icon_name("sidebar-show-symbolic");
    togglebut.set_icon_name("view-dual-symbolic");
    //togglebut.set_label("Categories");

    togglebut
        .bind_property("active", &osv, "show-sidebar")
        .bidirectional()
        .sync_create()
        .build();
    osv.set_show_sidebar(true);

    headerbar.pack_start(&togglebut);
    tbv.set_content(Some(&osv));

    //
    let (sender, receiver) = async_channel::unbounded();
    // gtk onload

    let gtksender1 = sender.clone();
    let gtkviewstack1 = viewstack.clone();
    gtkmenu.connect_realize(move |_gtkmenu| {
        println!("GtkMenu Request Raised..");
        // The main loop executes the asynchronous block
        gtkviewstack1.set_visible_child_name("contentboxloading");
        if gtkviewstack1.child_by_name("gtkcontentbox-page1").is_some(){
            gtkviewstack1.set_visible_child_name("gtkcontentbox-page1");
            return;
        }
        glib::spawn_future_local(clone!(@strong gtksender1 => async move {
            glib::timeout_future_seconds(3).await;
            let pagenoval: &str = "1";
            let orderbyval: &str = "latest";
            println!("Gtk4 Raised..");
            let gtkscroll_flowbox = create_catalog(LinkType::GtkTheme, pagenoval, orderbyval);
            println!("Gtk4 reponse came..");
            gtksender1.send((LinkType::GtkTheme, gtkscroll_flowbox)).await.expect("The channel needs to be open.");
        }));
    });

    // gtk onclick
    let gtksender = sender.clone();
    let gtkviewstack = viewstack.clone();
    gtkmenu.connect_activated(move |_gtkmenu| {
        println!("GtkMenu Request Raised..");
        // The main loop executes the asynchronous block
        gtkviewstack.set_visible_child_name("contentboxloading");
        if gtkviewstack.child_by_name("gtkcontentbox-page1").is_some(){
            gtkviewstack.set_visible_child_name("gtkcontentbox-page1");
            return;
        }
        glib::spawn_future_local(clone!(@strong gtksender => async move {
            glib::timeout_future_seconds(3).await;
            let pagenoval: &str = "1";
            let orderbyval: &str = "latest";
            println!("Gtk4 Raised..");
            let gtkscroll_flowbox = create_catalog(LinkType::GtkTheme, pagenoval, orderbyval);
            println!("Gtk4 reponse came..");
            gtksender.send((LinkType::GtkTheme, gtkscroll_flowbox)).await.expect("The channel needs to be open.");
        }));
    });

    //let (iconsender, iconreceiver) = async_channel::unbounded();
    let iconsender = sender.clone();
    let iconviewstack = viewstack.clone();
    iconmenu.connect_activated(move |_| {
        println!("IconMenu Request Raised..");
        // The main loop executes the asynchronous block
        iconviewstack.set_visible_child_name("contentboxloading");
        if iconviewstack.child_by_name("iconcontentbox-page1").is_some(){
            iconviewstack.set_visible_child_name("iconcontentbox-page1");
            return;
        }
        glib::spawn_future_local(clone!(@strong iconsender => async move {
            glib::timeout_future_seconds(3).await;
            let pagenoval: &str = "1";
            let orderbyval: &str = "latest";
            println!("Icon Raised..");
            let iconscroll_flowbox = create_catalog(LinkType::IconTheme, pagenoval, orderbyval);
            iconsender.send((LinkType::IconTheme, iconscroll_flowbox)).await.expect("The channel needs to be open.");
        }));
    });

    //let (iconsender, iconreceiver) = async_channel::unbounded();
    let cursorsender = sender.clone();
    let cursorviewstack = viewstack.clone();
    cursormenu.connect_activated(move |_| {
        println!("IconMenu Request Raised..");
        // The main loop executes the asynchronous block
        cursorviewstack.set_visible_child_name("contentboxloading");
        if cursorviewstack.child_by_name("cursorcontentbox-page1").is_some(){
            cursorviewstack.set_visible_child_name("cursorcontentbox-page1");
            return;
        }
        glib::spawn_future_local(clone!(@strong cursorsender => async move {
            glib::timeout_future_seconds(3).await;
            let pagenoval: &str = "1";
            let orderbyval: &str = "latest";
            println!("Cursor Raised..");
            let cursorscroll_flowbox = create_catalog(LinkType::CursorTheme, pagenoval, orderbyval);
            cursorsender.send((LinkType::CursorTheme, cursorscroll_flowbox)).await.expect("The channel needs to be open.");
        }));
        println!("End Function");
    });

    // The main loop executes the asynchronous block

    glib::spawn_future_local(async move {
        println!("scroll Request Raised..");
        while let Ok(recv) = receiver.recv().await {
            println!("scroll_gtkflowbox Request Raised..");
            let link_type = recv.0;
            let scroll_flowbox = recv.1;
            let pagename = match link_type {
                LinkType::IconTheme => "iconcontentbox-page1",
                LinkType::GtkTheme => "gtkcontentbox-page1",
                LinkType::CursorTheme => "cursorcontentbox-page1",
                _ => "Invalid",
            };
            match viewstack.child_by_name(&pagename) {
                None => {
                    viewstack.add_named(&scroll_flowbox, Some(&pagename));
                    viewstack.set_visible_child_name(&pagename);
                    println!("ViewStackPAge not found for Contentpage1 connect_Activate");
                }
                _ => {
                    viewstack.set_visible_child_name(&pagename);
                    println!("ViewStackPAge found for Contentpage1 connect_Activate");
                }
            }
        }
    });

    // Present window
    let window = ApplicationWindow::builder()
        .application(app)
        .content(&tbv)
        .build();

    window.present();
}
